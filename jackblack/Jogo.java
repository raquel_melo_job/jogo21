package com.company.dia105.jogo21.jackblack;

import java.util.*;

public class Jogo {
    private int maiorPontuacao;
    private int pontuacaoAtual;
    private HashMap mao;
    private IO in;
    private Baralho baralho;

    public Jogo() {
        this.maiorPontuacao = 0;
        this.pontuacaoAtual = 0;
        this.in = new IO();
        this.mao = new HashMap<>();
        this.baralho = new Baralho();
    }

    public void inicio(HashMap<String, Integer> baralho, boolean vontade){
        do{
            this.reiniciarJogo();
            execucao(baralho,vontade);
            int resposta = in.desejaJogarNovamente();
            if (resposta==2){
                vontade = false;
            }
        }while (vontade);
    }

    public void execucao(HashMap<String, Integer> baralho, boolean vontade){
        boolean flag;
        this.pontuacaoAtual = 0;
        do{
            this.retirarCartaAleatoria(baralho);
            flag = this.checagemPontuacao();
            if(!flag){
                this.isMaiorPontuacao(pontuacaoAtual);
                in.mostrarMaiorPontuacao(this.maiorPontuacao);
                break;
            }else{
                int resposta = in.perguntaSeContinuar();
                if (resposta==2){
                    vontade = false;
                    this.isMaiorPontuacao(this.pontuacaoAtual);
                    in.mostrarPontuacoes(this.pontuacaoAtual,this.maiorPontuacao);
                }
            }
        }while (vontade);
    }

    public void retirarCartaAleatoria(HashMap<String,Integer> baralho){
        Random gerador = new Random();
        int posicao = gerador.nextInt(baralho.size());

        ArrayList<String> chaves = new ArrayList<>();

        for (String key: baralho.keySet()){
            chaves.add(key);
        }

        this.mao.put(chaves.get(posicao),baralho.get(chaves.get(posicao)));

        somarPontuacao(baralho.get(chaves.get(posicao)));

        baralho.remove(baralho.get(chaves.get(posicao)));

        in.maoAtual(mao);
        in.pontuacaoAtual(this.pontuacaoAtual);
    }

    public void somarPontuacao(int pontuacaoAtual){
        this.pontuacaoAtual+=pontuacaoAtual;
    }

    public void isMaiorPontuacao(int pontuacaoAtual){
        if (pontuacaoAtual > this.maiorPontuacao){
            this.maiorPontuacao = pontuacaoAtual;
        }
    }

    public boolean checagemPontuacao(){
        if (this.pontuacaoAtual>21){
            in.mostrarErroPontuacao();
            return false;
        }
        return true;
    }

    public void reiniciarJogo(){
        this.baralho = new Baralho();
        this.mao.clear();
    }
}
