package com.company.dia105.jogo21.jackblack;

import java.util.HashMap;
import java.util.Scanner;

public class IO {
    Scanner in;

    public IO() {
        this.in = new Scanner(System.in);
    }

    public int perguntaSeContinuar(){
        System.out.print("Digite 1 para outra carta ou 2 para parar: ");
        return this.in.nextInt();
    }

    public void maoAtual(HashMap mao){
        System.out.println("Suas cartas atuais são: "+mao);
    }

    public int desejaJogarNovamente(){
        System.out.print("Digite 1 para jogar novamente ou 2 para parar: ");
        return this.in.nextInt();
    }

    public void pontuacaoAtual(int pontuacao){
        System.out.println("Sua pontuação atual é: " +pontuacao);
    }

    public void mostrarPontuacoes(int pontuacaoAtual, int maiorPontuacao){
        System.out.println("Sua pontuação nesta rodada foi "+ pontuacaoAtual+ "\n sua maior pontuacao atualmente é "+maiorPontuacao);
    }

    public void mostrarErroPontuacao() {
        System.out.println("Sua pontuação ultrapassou 21. PERDEU");
    }

    public void mostrarMaiorPontuacao(int maiorPontuacao) {
        System.out.println("sua maior pontuacao atualmente é " +maiorPontuacao);
    }
}
