package com.company.dia105.jogo21.jackblack;

import java.util.HashMap;

public class Jogador {
    Baralho baralho;

    public Jogador() {
        this.baralho = new Baralho();
    }

    public void iniciarJogo(){
        Jogo novoJogo = new Jogo();
        HashMap<String, Integer> baralho = this.baralho.criarBaralho();
        novoJogo.inicio(baralho,true);
    }
}
