package com.company.dia105.jogo21.jackblack;

import java.util.HashMap;

public class Baralho {

    public HashMap<String,Integer> criarBaralho() {
        HashMap<String,Integer> cartas = new HashMap<>();
        String[] nomes = {"Paus", "Ouro", "Copas", "Espadas"};

        for (int j = 1; j <= 13; j++) {
            for (int i = 0; i <= 3; i++) {
                if(j==1){
                    cartas.put("Às de " + nomes[i],1);
                }else if(j==11){
                    cartas.put("Valete de " + nomes[i],10);
                }else if(j==12){
                    cartas.put("Dama de " + nomes[i],10);
                }else if(j==13){
                    cartas.put("Rei de " + nomes[i],10);
                }else{
                    cartas.put(j + " de " + nomes[i],j);
                }
            }
        }
        return cartas;
    }
}
